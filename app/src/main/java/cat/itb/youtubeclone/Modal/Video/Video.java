package cat.itb.youtubeclone.Modal.Video;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cat.itb.youtubeclone.Database.FirebaseDataItem;

public class Video extends FirebaseDataItem implements Serializable
{

	@SerializedName("snippet")
	private Snippet snippet;

	@SerializedName("kind")
	private String kind;

	@SerializedName("etag")
	private String etag;

	@SerializedName("contentDetails")
	private ContentDetails contentDetails;

	@SerializedName("statistics")
	private Statistics statistics;

	@SerializedName("status")
	private Status status;

	public void setSnippet(Snippet snippet){
		this.snippet = snippet;
	}

	public Snippet getSnippet(){
		return snippet;
	}

	public void setKind(String kind){
		this.kind = kind;
	}

	public String getKind(){
		return kind;
	}

	public void setEtag(String etag){
		this.etag = etag;
	}

	public String getEtag(){
		return etag;
	}

	public void setContentDetails(ContentDetails contentDetails){
		this.contentDetails = contentDetails;
	}

	public ContentDetails getContentDetails(){
		return contentDetails;
	}

	public void setStatistics(Statistics statistics){
		this.statistics = statistics;
	}

	public Statistics getStatistics(){
		return statistics;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"VideosItem{" + 
			"snippet = '" + snippet + '\'' + 
			",kind = '" + kind + '\'' + 
			",etag = '" + etag + '\'' +
			",contentDetails = '" + contentDetails + '\'' + 
			",statistics = '" + statistics + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}