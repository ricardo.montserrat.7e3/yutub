package cat.itb.youtubeclone.Modal.Video;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ContentDetails implements Serializable {

	@SerializedName("duration")
	private String duration;

	@SerializedName("aspectRatio")
	private String aspectRatio;

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setAspectRatio(String aspectRatio){
		this.aspectRatio = aspectRatio;
	}

	public String getAspectRatio(){
		return aspectRatio;
	}

	@Override
 	public String toString(){
		return 
			"ContentDetails{" + 
			"duration = '" + duration + '\'' + 
			",aspectRatio = '" + aspectRatio + '\'' + 
			"}";
		}
}