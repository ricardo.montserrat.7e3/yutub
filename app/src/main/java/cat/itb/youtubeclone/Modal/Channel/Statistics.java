package cat.itb.youtubeclone.Modal.Channel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cat.itb.youtubeclone.Utilities.Utilities;

public class Statistics implements Serializable {

	@SerializedName("videoCount")
	private String videoCount;

	@SerializedName("subscriberCount")
	private String subscriberCount;

	@SerializedName("viewCount")
	private String viewCount;

	@SerializedName("hiddenSubscriberCount")
	private boolean hiddenSubscriberCount;

	public void setVideoCount(String videoCount){
		this.videoCount = videoCount;
	}

	public String getVideoCount(){
		return videoCount;
	}

	public void setSubscriberCount(String subscriberCount){
		this.subscriberCount = subscriberCount;
	}

	public String getSubscriberCount(){
		return subscriberCount;
	}

	public String obtainSubscriberCount(){
		return Utilities.formatNumbers(subscriberCount);
	}

	public void setViewCount(String viewCount){
		this.viewCount = viewCount;
	}

	public String getViewCount(){
		return viewCount;
	}

	public void setHiddenSubscriberCount(boolean hiddenSubscriberCount){
		this.hiddenSubscriberCount = hiddenSubscriberCount;
	}

	public boolean isHiddenSubscriberCount(){
		return hiddenSubscriberCount;
	}

	public void subscribe() { subscriberCount = Utilities.addToStringInteger(subscriberCount, 1); }

	public void unsubscribe() { subscriberCount = Utilities.addToStringInteger(subscriberCount, -1); }

	@Override
 	public String toString(){
		return 
			"Statistics{" + 
			"videoCount = '" + videoCount + '\'' + 
			",subscriberCount = '" + subscriberCount + '\'' + 
			",viewCount = '" + viewCount + '\'' + 
			",hiddenSubscriberCount = '" + hiddenSubscriberCount + '\'' + 
			"}";
		}
}