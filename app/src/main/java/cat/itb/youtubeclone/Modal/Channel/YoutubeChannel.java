package cat.itb.youtubeclone.Modal.Channel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cat.itb.youtubeclone.Database.FirebaseDataItem;

public class YoutubeChannel extends FirebaseDataItem implements Serializable
{
	public enum ThumbnailQuality {low, medium, high}

	@SerializedName("snippet")
	private Snippet snippet;

	@SerializedName("kind")
	private String kind;

	@SerializedName("etag")
	private String etag;

	@SerializedName("statistics")
	private Statistics statistics;

	public void setSnippet(Snippet snippet){
		this.snippet = snippet;
	}

	public Snippet getSnippet(){
		return snippet;
	}

	public void setKind(String kind){
		this.kind = kind;
	}

	public String getKind(){
		return kind;
	}

	public void setEtag(String etag){
		this.etag = etag;
	}

	public String getEtag(){
		return etag;
	}

	public void setStatistics(Statistics statistics){
		this.statistics = statistics;
	}

	public Statistics getStatistics(){
		return statistics;
	}

	public String obtainThumbnailUrl(ThumbnailQuality quality){
		Thumbnails thumbnail = getSnippet().getThumbnails();
		switch (quality){
			case low:
				return thumbnail.getJsonMemberDefault().getUrl();
			case high:
				return thumbnail.getHigh().getUrl();
			default:
				return thumbnail.getMedium().getUrl();
		}
	}



	@Override
 	public String toString(){
		return 
			"Channel{" + 
			"snippet = '" + snippet + '\'' + 
			",kind = '" + kind + '\'' + 
			",etag = '" + etag + '\'' +
			",statistics = '" + statistics + '\'' + 
			"}";
		}
}