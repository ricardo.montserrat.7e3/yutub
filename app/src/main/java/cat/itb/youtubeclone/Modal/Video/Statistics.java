package cat.itb.youtubeclone.Modal.Video;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import cat.itb.youtubeclone.Utilities.Utilities;

public class Statistics implements Serializable
{

	@SerializedName("dislikeCount")
	private String dislikeCount;

	@SerializedName("likeCount")
	private String likeCount;

	@SerializedName("viewCount")
	private String viewCount;

	@SerializedName("favoriteCount")
	private String favoriteCount;

	@SerializedName("commentCount")
	private String commentCount;

	public void setDislikeCount(String dislikeCount){
		this.dislikeCount = dislikeCount;
	}

	public String getDislikeCount(){
		return dislikeCount;
	}

	public String obtainDislikeCount(){
		return Utilities.formatNumbers(dislikeCount);
	}

	public void setLikeCount(String likeCount){
		this.likeCount = likeCount;
	}

	public String getLikeCount(){
		return likeCount;
	}

	public String obtainLikeCount(){
		return Utilities.formatNumbers(likeCount);
	}

	public void setViewCount(String viewCount){
		this.viewCount = viewCount;
	}

	public String getViewCount(){
		return viewCount;
	}

	public String obtainViewCount(){
		return Utilities.formatNumbers(viewCount);
	}

	public void setFavoriteCount(String favoriteCount){
		this.favoriteCount = favoriteCount;
	}

	public String getFavoriteCount(){
		return favoriteCount;
	}

	public void setCommentCount(String commentCount){
		this.commentCount = commentCount;
	}

	public String getCommentCount(){
		return commentCount;
	}

	public void incrementViewCount() { viewCount = Utilities.addToStringInteger(viewCount, 1); }

	public void incrementLikeCount() { likeCount = Utilities.addToStringInteger(likeCount, 1); }

	public void decrementLikeCount() { likeCount = Utilities.addToStringInteger(likeCount, -1); }

	public void incrementDislikeCount() { dislikeCount = Utilities.addToStringInteger(dislikeCount, -1); }

	public void decrementDislikeCount() { dislikeCount = Utilities.addToStringInteger(dislikeCount, -1); }

	@Override
 	public String toString(){
		return 
			"Statistics{" + 
			"dislikeCount = '" + dislikeCount + '\'' + 
			",likeCount = '" + likeCount + '\'' + 
			",viewCount = '" + viewCount + '\'' + 
			",favoriteCount = '" + favoriteCount + '\'' + 
			",commentCount = '" + commentCount + '\'' + 
			"}";
		}
}