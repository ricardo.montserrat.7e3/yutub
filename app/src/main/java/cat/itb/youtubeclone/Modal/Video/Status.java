package cat.itb.youtubeclone.Modal.Video;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Status implements Serializable {

	@SerializedName("privacyStatus")
	private String privacyStatus;

	@SerializedName("uploadStatus")
	private String uploadStatus;

	public void setPrivacyStatus(String privacyStatus){
		this.privacyStatus = privacyStatus;
	}

	public String getPrivacyStatus(){
		return privacyStatus;
	}

	public void setUploadStatus(String uploadStatus){
		this.uploadStatus = uploadStatus;
	}

	public String getUploadStatus(){
		return uploadStatus;
	}

	@Override
 	public String toString(){
		return 
			"Status{" + 
			"privacyStatus = '" + privacyStatus + '\'' + 
			",uploadStatus = '" + uploadStatus + '\'' + 
			"}";
		}
}