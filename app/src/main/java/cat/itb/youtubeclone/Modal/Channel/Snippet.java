package cat.itb.youtubeclone.Modal.Channel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Snippet implements Serializable {

	@SerializedName("customUrl")
	private String customUrl;

	@SerializedName("publishedAt")
	private String publishedAt;

	@SerializedName("localized")
	private Localized localized;

	@SerializedName("description")
	private String description;

	@SerializedName("title")
	private String title;

	@SerializedName("thumbnails")
	private Thumbnails thumbnails;

	public void setCustomUrl(String customUrl){
		this.customUrl = customUrl;
	}

	public String getCustomUrl(){
		return customUrl;
	}

	public void setPublishedAt(String publishedAt){
		this.publishedAt = publishedAt;
	}

	public String getPublishedAt(){
		return publishedAt;
	}

	public void setLocalized(Localized localized){
		this.localized = localized;
	}

	public Localized getLocalized(){
		return localized;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setThumbnails(Thumbnails thumbnails){
		this.thumbnails = thumbnails;
	}

	public Thumbnails getThumbnails(){
		return thumbnails;
	}

	@Override
 	public String toString(){
		return 
			"Snippet{" + 
			"customUrl = '" + customUrl + '\'' + 
			",publishedAt = '" + publishedAt + '\'' + 
			",localized = '" + localized + '\'' + 
			",description = '" + description + '\'' + 
			",title = '" + title + '\'' + 
			",thumbnails = '" + thumbnails + '\'' + 
			"}";
		}
}