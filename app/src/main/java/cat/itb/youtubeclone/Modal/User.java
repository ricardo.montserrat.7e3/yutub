package cat.itb.youtubeclone.Modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import cat.itb.youtubeclone.Database.FirebaseDataItem;

public class User extends FirebaseDataItem implements Serializable
{
    private String m_Username, m_Password, m_Email;
    private List<String> m_Subscriptions = new ArrayList<>(), m_LikedVideos = new ArrayList<>(), m_DislikedVideos = new ArrayList<>();
    private HashSet<String> m_History = new HashSet<>(), m_Watchlist = new HashSet<>();

    public User() { }

    public User(String m_Username, String m_Password, String m_Email) { this.m_Username = m_Username; this.m_Password = m_Password; this.m_Email = m_Email; }

    public String getUsername() { return m_Username; }

    public void setUsername(String m_Username) { this.m_Username = m_Username; }

    public String getPassword() { return m_Password; }

    public void setPassword(String m_Password) { this.m_Password = m_Password; }

    public String getEmail() { return m_Email; }

    public void setEmail(String m_Email) { this.m_Email = m_Email; }

    public List<String> getHistory()
    {
        return new ArrayList<>(m_History);
    }

    public void setHistory(List<String> m_History)
    {
        this.m_History.addAll(m_History);
    }

    public List<String> getWatchlist()
    {
        return new ArrayList<>(m_Watchlist);
    }

    public void setWatchlist(List<String> m_Watchlist)
    {
        this.m_Watchlist.addAll(m_Watchlist);
    }

    public List<String> getSubscriptions() { return m_Subscriptions; }

    public void setSubscriptions(List<String> subscriptions) { this.m_Subscriptions = subscriptions; }

    public List<String> getLikedVideos() { return m_LikedVideos; }

    public void setLikedVideos(List<String> likedVideos) { this.m_LikedVideos = likedVideos; }

    public List<String> getDislikedVideos() { return m_DislikedVideos; }

    public void setDislikedVideos(List<String> dislikedVideos) { this.m_DislikedVideos = dislikedVideos; }

    public boolean isLiked(String videoID) { return m_LikedVideos.contains(videoID); }

    public boolean isDisliked(String videoID) { return m_DislikedVideos.contains(videoID); }

    public boolean isSubscribed(String channelID) { return m_Subscriptions.contains(channelID); }

    public void unsubscribe(String channelID) { m_Subscriptions.remove(channelID); }

    public void subscribe(String channelID) { m_Subscriptions.add(channelID); }

    public void like(String videoID) { m_LikedVideos.add(videoID); }

    public void unlike(String videoID) { m_LikedVideos.remove(videoID); }

    public void dislike(String videoID) { m_DislikedVideos.add(videoID); }

    public void unalike(String videoID) { m_DislikedVideos.remove(videoID); }

    public void addHistory(String id) { m_History.add(id); }

    public void addWatchLater(String id) { m_Watchlist.add(id); }

    public boolean inWatchlist(String id) { return  m_Watchlist.contains(id); }
}
