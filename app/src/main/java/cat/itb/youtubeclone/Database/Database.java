package cat.itb.youtubeclone.Database;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cat.itb.youtubeclone.Utilities.Utilities;

public class Database<T extends FirebaseDataItem>
{
    public String databaseRef, storageRef;

    private DatabaseReference m_DatabaseReference;
    private StorageReference m_StorageReference;

    //Firebase data type
    private final Class<T> myClass;

    public Database(String databaseRef, String storageRef, Class<T> typeClass)
    {
        this.databaseRef = databaseRef;
        this.storageRef = storageRef;
        myClass = typeClass;
    }

    /**
     * Initializes the database by getting instances' References
     */
    public void initialize()
    {
        m_DatabaseReference = FirebaseDatabase.getInstance().getReference(databaseRef);
        m_StorageReference = FirebaseStorage.getInstance().getReference().child(storageRef);
    }

    /**
     * Updates the values of the firebase by passing an object of the same type
     *
     * @param firebaseItem the object of the firebase type
     * @return id of the firebaseitem
     */
    public String updateDatabase(T firebaseItem)
    {
        String id = firebaseItem.getId();
        if (id == null || id.isEmpty())
        {
            id = m_DatabaseReference.push().getKey();
            firebaseItem.setId(id);
        }
        assert id != null;
        m_DatabaseReference.child(id).setValue(firebaseItem);
        return id;
    }

    /**
     * Updates the property of an object on the firebase by id
     *
     * @param id            id of the object to be updated
     * @param propertyName  name of property to update of the object
     * @param propertyValue new value of the property
     */
    public void updateDatabasePropertyOf(String id, String propertyName, Object propertyValue) { m_DatabaseReference.child(id).child(propertyName).setValue(propertyValue); }

    /**
     * Removes an object by id of the firebase
     *
     * @param id object id
     */
    public void remove(String id) { m_DatabaseReference.child(id).removeValue(); }

    /**
     * Gets an object by id of the firebase
     *
     * @param data_id     object id
     * @param onCompleted the actions you will do to the data passed
     */
    public void getData(String data_id, Utilities.GetDataListener<T> onCompleted)
    {
        m_DatabaseReference.child(data_id).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) { onCompleted.onSuccess(dataSnapshot.getValue(myClass)); m_DatabaseReference.child(data_id).removeEventListener(this);}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    /**
     * Inside the listener, returns a boolean whether a property with the passed value exist on the database
     *
     * @param property      property name on firebase
     * @param value         value to look for
     * @param whenCompleted the actions you will do to the data passed
     */
    public void existPropertyWithValue(String property, String value, Utilities.GetDataListener<Boolean> whenCompleted)
    {
        m_DatabaseReference.orderByChild(property).equalTo(value).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) { whenCompleted.onSuccess(snapshot.exists()); m_DatabaseReference.removeEventListener(this); }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    /**
     * Inside the listener, returns an object of type of the database, where its property is equal to the value passed
     *
     * @param property      property name on firebase of the object
     * @param value         the value to be compared to
     * @param whenCompleted the actions you will do to the data passed
     */
    public void getDataWhere(String property, String value, Utilities.GetDataListener<T> whenCompleted)
    {
        m_DatabaseReference.orderByChild(property).equalTo(value).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                if (snapshot.getChildrenCount() == 0) whenCompleted.onSuccess(null);
                for (DataSnapshot snap : snapshot.getChildren()) { whenCompleted.onSuccess(snap.getValue(myClass)); }
                m_DatabaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    /**
     * Inside the listener, returns an object of type of the database, where its property is equal to the value passed
     *
     * @param property      property name on firebase of the object
     * @param value         the value to be compared to
     * @param whenCompleted the actions you will do to the data passed
     */
    public void getDataWhere(String property, String value, Utilities.GetDataListener<List<T>> whenCompleted, boolean asList)
    {
        m_DatabaseReference.orderByChild(property).equalTo(value).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                List<T> data = new ArrayList<>();
                snapshot.getChildren().forEach(x -> data.add(x.getValue(myClass)));
                whenCompleted.onSuccess(data);
                m_DatabaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) { }
        });
    }

    /**
     * Inside the listener, returns a list of the object type of the database
     *
     * @param listener the actions you will do to the data passed
     */
    public void getAllData(Utilities.GetDataListener<List<T>> listener)
    {
        m_DatabaseReference.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                List<T> values = new ArrayList<>();
                dataSnapshot.getChildren().forEach(x -> values.add(x.getValue(myClass)));
                listener.onSuccess(values);
                m_DatabaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }
}
