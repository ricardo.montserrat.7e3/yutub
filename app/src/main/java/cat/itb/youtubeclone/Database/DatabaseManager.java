package cat.itb.youtubeclone.Database;

import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.User;
import cat.itb.youtubeclone.Modal.Video.Video;

public class DatabaseManager
{
    public static final String DATABASE_USERS = "Users";
    public static final String DATABASE_VIDEOS = "Videos";
    public static final String DATABASE_CHANNELS = "Youtube Channels";

    public DatabaseManager() { }

    public static<T extends FirebaseDataItem> Database<T> getDatabase(Class<T> type)
    {
        if (Video.class.equals(type))
        {
            return new Database<T>(DATABASE_VIDEOS, DATABASE_VIDEOS + "Archives", type);
        }
        else if (User.class.equals(type))
        {
            return new Database<T>(DATABASE_USERS, DATABASE_USERS + "Archives", type);
        }
        else if (YoutubeChannel.class.equals(type))
        {
            return new Database<T>(DATABASE_CHANNELS, DATABASE_CHANNELS + "Archives", type);
        }
        return null;
    }
}
