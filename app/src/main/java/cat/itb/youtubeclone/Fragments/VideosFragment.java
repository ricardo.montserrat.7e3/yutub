package cat.itb.youtubeclone.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;

public class VideosFragment extends Fragment
{
    public VideosFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.layout_fragment_videos_list, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        MainActivity.instance.videosDatabase.getAllData(list ->
        {
            Collections.shuffle(list);

            if(MainActivity.instance.adapter == null) MainActivity.instance.adapter = new VideoRecyclerViewAdapter(list);
            else MainActivity.instance.adapter.setVideos(list);

            recyclerView.setAdapter(MainActivity.instance.adapter);
        });

        return view;
    }
}