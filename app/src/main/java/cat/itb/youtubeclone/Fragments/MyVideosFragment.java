package cat.itb.youtubeclone.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;

public class MyVideosFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_my_videos, container, false);

        if (!MainActivity.instance.isLoggedIn()) ((TextView) v.findViewById(R.id.textview_myvideos_not_uploaded)).setText(getText(R.string.necesitas_estar_logueado));

        return v;
    }
}