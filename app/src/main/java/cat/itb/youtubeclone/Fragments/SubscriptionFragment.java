package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cat.itb.youtubeclone.Adapters.ChannelImagesRecyclerViewAdapter;
import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.R;


public class SubscriptionFragment extends Fragment
{
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_subscription, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview_subscription_videos);
        RecyclerView channel_recyclerView = v.findViewById(R.id.recyclerview_subscription_icons);
        ChannelImagesRecyclerViewAdapter channel_adapter;

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        channel_recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        VideoRecyclerViewAdapter videoListAdapter = new VideoRecyclerViewAdapter(new ArrayList<>());

        recyclerView.setAdapter(videoListAdapter);

        List<String> subscriptions = MainActivity.instance.loggedUser.getSubscriptions();
        if (subscriptions.size() > 0)
        {
            subscriptions.forEach(channelId ->
                    MainActivity.instance.videosDatabase.getDataWhere("/snippet/channelId", channelId, list ->
                    {
                        Collections.shuffle(list);
                        list.forEach(videoListAdapter::addVideoNoUpdate);
                        videoListAdapter.updateVideoList();
                    }, true));

            List<YoutubeChannel> channelList = new ArrayList<>();
            channel_adapter = new ChannelImagesRecyclerViewAdapter(channelList);
            channel_recyclerView.setAdapter(channel_adapter);

            for (String channelId : subscriptions) MainActivity.instance.channelsDatabase.getData(channelId, channel_adapter::addChannel);
        }
        else
        {
            v.findViewById(R.id.textview_not_suscribed).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        return v;
    }
}
