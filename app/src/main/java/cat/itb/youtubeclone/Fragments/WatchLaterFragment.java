package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;

public class WatchLaterFragment extends Fragment
{

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_watch_later, container, false);

        TextView notSeenVideoTextView = v.findViewById(R.id.textview_atchlater_not_watched);

        MainActivity main = MainActivity.instance;

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview_watchlater_videos);

        if(main.isLoggedIn())
        {

            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            VideoRecyclerViewAdapter videoListAdapter = new VideoRecyclerViewAdapter(new ArrayList<>());

            recyclerView.setAdapter(videoListAdapter);

            v.findViewById(R.id.imageview_watchlater_shuffle).setOnClickListener(x ->
            {
                int size = videoListAdapter.getItemCount();
                if(size > 0) videoListAdapter.clickOn((int) (Math.random() * size));
            });

            List<String> watchList = main.loggedUser.getWatchlist();

            if(watchList.size() > 0)
            {
                Database<Video> videoDatabase = main.videosDatabase;
                watchList.forEach(videoId -> videoDatabase.getData(videoId, videoListAdapter::addVideo));
            }
            else
            {
                notSeenVideoTextView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
        else
        {
            notSeenVideoTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            notSeenVideoTextView.setText(getText(R.string.necesitas_estar_logueado));
        }

        return v;
    }
}