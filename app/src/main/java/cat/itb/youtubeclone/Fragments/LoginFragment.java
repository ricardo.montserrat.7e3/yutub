package cat.itb.youtubeclone.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;

public class LoginFragment extends Fragment
{
    private TextInputEditText m_UserUsername, m_UserPassword;

    private MainActivity.FragmentTag m_LastFragment;

    public static final String TAG_SERIALIZABLE = "lastFragment";

    public static final String SHARED_TAG_USER = "user", SHARED_TAG_PASSWORD = "password";

    public LoginFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_login, container, false);

        assert getArguments() != null;
        m_LastFragment = (MainActivity.FragmentTag) getArguments().getSerializable(TAG_SERIALIZABLE);

        m_UserUsername = v.findViewById(R.id.textinput_login_user);
        m_UserPassword = v.findViewById(R.id.textinput_password_user);

        v.findViewById(R.id.button_login).setOnClickListener(x -> login());

        v.findViewById(R.id.button_goto_register).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Register, m_LastFragment, TAG_SERIALIZABLE));

        v.findViewById(R.id.imagebutton_login_return).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Videos));

        v.findViewById(R.id.textview_login_forgot_password).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.RetrievePassword));

        SharedPreferences preferences = MainActivity.instance.getPreferences(Context.MODE_PRIVATE);

        m_UserUsername.setText(preferences.getString(SHARED_TAG_USER, ""));
        m_UserPassword.setText(preferences.getString(SHARED_TAG_PASSWORD, ""));
        return v;
    }

    private void login()
    {
        if (Utilities.isEmpty(m_UserUsername, "¡Por favor, introduce tu usuario!")
                || Utilities.isEmpty(m_UserPassword, "¡Por favor, introduce tu contraseña!")) return;

        MainActivity.instance.usersDatabase.getDataWhere("username", Objects.requireNonNull(m_UserUsername.getText()).toString(), u ->
        {
            if (u != null)
            {
                if (u.getPassword().equals(Objects.requireNonNull(m_UserPassword.getText()).toString()))
                {
                    MainActivity main = MainActivity.instance;

                    main.loggedUser = u;
                    main.goToFragment(m_LastFragment);
                    main.updateDrawerHeader(main.findViewById(R.id.drawer_navigation_view));

                    Utilities.ShowText(getContext(), "Bienvenido, " + u.getUsername() + "!");

                    //Saving last logged in
                    SharedPreferences.Editor editor = main.getPreferences(Context.MODE_PRIVATE).edit();

                    editor.putString(SHARED_TAG_USER, u.getUsername());
                    editor.putString(SHARED_TAG_PASSWORD, u.getPassword());

                    editor.apply();
                }
                else
                {
                    m_UserPassword.setError(getResources().getString(R.string.login_error));
                    Utilities.ShowText(getContext(), "La contraseña correcta es " + u.getPassword());
                }
            }
            else m_UserUsername.setError(getResources().getString(R.string.login_error));
        });
    }
}