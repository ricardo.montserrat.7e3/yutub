package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.MailJob;
import cat.itb.youtubeclone.Utilities.Utilities;


public class RecuperarPasswordFragment extends Fragment
{
    private TextInputEditText m_TextInput, m_TextInputRepeatPassword;
    private TextInputLayout m_TextInputLayout;

    private TextView m_TitleView;
    private MaterialButton m_ButtonNext;

    private String m_UserEmail;
    private final String m_UserCode = Utilities.generateCode(4);

    private final String EMAIL_CONTENT = "Este es tu código de verificación, introdúcelo en la aplicación\n\n" + m_UserCode;
    private boolean m_EmailSent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_recuperar_password, container, false);

        m_TextInput = v.findViewById(R.id.textinput_rec_password_email);
        m_TextInputLayout = v.findViewById(R.id.textinputlayout_rec_password_email);
        m_TitleView = v.findViewById(R.id.textview_rec_password);
        m_ButtonNext = v.findViewById(R.id.button_rec_password);
        m_TextInputRepeatPassword = v.findViewById(R.id.textinput_rec_repeat_password);

        Bundle values = getArguments();

        if (values != null)
        {
            ConfigurationFragment.ConfigOption option = (ConfigurationFragment.ConfigOption) values.getSerializable(ConfigurationFragment.CONFIG_OPTION_TAG);

            if (option == ConfigurationFragment.ConfigOption.ChangeUsername)
            {
                setViews("Write new username", "New Username", "Change Username" , x ->
                {
                    if(!Utilities.isEmpty(m_TextInput, "Please, write your new username"))
                    {
                        MainActivity main = MainActivity.instance;
                        main.loggedUser.setUsername(Objects.requireNonNull(m_TextInput.getText()).toString());
                        main.updateUserData();
                    }
                });
            }
            else
            {
                m_TextInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                setViews("Write new password", "New Password", "Change Password" , x ->
                {
                    if(!Utilities.isEmpty(m_TextInput, "Please, write your new password")
                    && !Utilities.isEmpty(m_TextInputRepeatPassword, "Please, write the password again!"))
                    {
                        MainActivity main = MainActivity.instance;
                        main.loggedUser.setPassword(Objects.requireNonNull(m_TextInput.getText()).toString());
                        main.updateUserData();
                    }
                });
            }


            v.findViewById(R.id.imagebutton_recuperar_password_return).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Videos));

            return v;
        }

        m_ButtonNext.setOnClickListener(button ->
        {
            m_UserEmail = Objects.requireNonNull(m_TextInput.getText()).toString();
            if (Utilities.isEmpty(m_TextInput, "¡Introduce un email!")) return;

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
            {
                MainActivity.instance.usersDatabase.existPropertyWithValue("email", m_UserEmail, exist ->
                {
                    if (exist)
                    {
                        sendRetrivePasswordEmail();

                        //If email was sent already, don't add another text listener to the input
                        if (m_EmailSent) return;

                        cleanInput();

                        //Recycle inputs and textviews by changing names
                        m_TextInputLayout.setHint(getResources().getString(R.string.introduce_codigo_recuperar_hint));

                        m_TitleView.setText(getResources().getString(R.string.introduce_codigo_recuperar));
                        m_ButtonNext.setText(getResources().getString(R.string.reenviar_correo_recuperar));

                        //Add code detection listener and comparer
                        m_TextInput.addTextChangedListener(new TextWatcher()
                        {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count)
                            {
                                if (m_TextInput.getText().toString().equals(m_UserCode))
                                {
                                    //Set everything for changing the password
                                    cleanInput();

                                    m_ButtonNext.setText(getResources().getString(R.string.siguiente));

                                    m_TextInputLayout.setHint(getResources().getString(R.string.introduce_nueva_contrasena_recuperar));

                                    v.findViewById(R.id.textinputlayout_rec_password_repeat).setVisibility(View.VISIBLE);

                                    m_TextInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

                                    m_ButtonNext.setOnClickListener(x ->
                                    {
                                        if (Utilities.isEmpty(m_TextInput, "Introduce la nueva contraseña!") || Utilities.isEmpty(m_TextInputRepeatPassword, "Por favor, repite tu contraseña")) return;

                                        String newPassword = m_TextInput.getText().toString();
                                        if (!newPassword.equals(Objects.requireNonNull(m_TextInputRepeatPassword.getText()).toString()))
                                            m_TextInputRepeatPassword.setError("¡La contraseña no es igual!");

                                        MainActivity.instance.usersDatabase.getDataWhere("email", m_UserEmail, user ->
                                        {
                                            if (user != null)
                                            {
                                                MainActivity.instance.usersDatabase.updateDatabasePropertyOf(user.getId(), "password", m_TextInput.getText().toString());
                                                MainActivity.instance.goToFragment(MainActivity.FragmentTag.Login);
                                            }
                                        });
                                    });

                                    m_TextInput.removeTextChangedListener(this);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) { }
                        });

                        m_EmailSent = true;
                    }
                    else m_TextInput.setError("El correo no esta registrado!");
                });
            }
        });

        v.findViewById(R.id.imagebutton_recuperar_password_return).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Login));

        return v;
    }

    private void setViews(String title, String hint, String buttonText, View.OnClickListener clickListener)
    {
        m_TitleView.setText(title);

        m_TextInputLayout.setHint(hint);

        m_ButtonNext.setText(buttonText);
        m_ButtonNext.setOnClickListener(clickListener);
    }

    private void cleanInput() { m_TextInput.setText(""); }

    private void sendRetrivePasswordEmail()
    {
        final String PENGUINS_MAFIA_EMAIL = "YutuB.customerservice@gmail.com";
        new MailJob(PENGUINS_MAFIA_EMAIL, "penguinsmafia3").execute(new MailJob.Mail(PENGUINS_MAFIA_EMAIL, m_UserEmail, "YutuB, restablece tu contraseña", EMAIL_CONTENT));
    }
}