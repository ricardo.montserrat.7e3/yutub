package cat.itb.youtubeclone.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.Collections;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.User;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;
import cat.itb.youtubeclone.Utilities.YoutubeManager;


public class WatchingVideoFragment extends Fragment
{
    private String m_VideoUrl = "Wr2YSThGxfQ";

    private YoutubeChannel m_VideoChannel;

    private TextView m_TextViewDescription, m_SubscribeText, m_SubscribersCountText;

    private ImageView m_LikedButton, m_DislikeButton;

    private TextView m_NumberOfLikes, m_NumberOfDislikes;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_watching_video, container, false);

        //VARIABLES INITIALIZATION
        MainActivity main = MainActivity.instance;

        m_TextViewDescription = v.findViewById(R.id.textview_watching_description);

        m_LikedButton = v.findViewById(R.id.imageview_watching_like);
        m_DislikeButton = v.findViewById(R.id.imageview_watching_dislike);

        m_SubscribeText = v.findViewById(R.id.subscription_message);

        m_SubscribersCountText = v.findViewById(R.id.channel_subscribers);

        //CHECKING FOR VIDEO VALUE PASSED
        Bundle values = getArguments();

        if (values != null)
        {
            Video video = (Video) values.getSerializable("video");

            assert video != null;
            m_VideoUrl = video.getId();

            video.getStatistics().incrementViewCount();

            if (main.isLoggedIn())
            {
                User user = main.loggedUser;
                String id = video.getId();

                m_SubscribeText.setText(getResources().getString(user.isSubscribed(id) ? R.string.subscrito : R.string.suscribirme));

                showDislikeButton(user.isDisliked(id));
                showLikeButton(user.isLiked(id));

                user.addHistory(id);

                main.updateUserData();
            }

            //DESCRIPTION AND TITLE SETTING
            MaterialButton buttonTitle = v.findViewById(R.id.video_player_title);
            buttonTitle.setText(video.getSnippet().getTitle());
            buttonTitle.setOnClickListener(button ->
            {
                if (m_TextViewDescription.getVisibility() == View.GONE)
                {
                    buttonTitle.setIcon(ContextCompat.getDrawable(requireContext(), R.drawable.ic_plegar));
                    m_TextViewDescription.setVisibility(View.VISIBLE);
                }
                else
                {
                    buttonTitle.setIcon(ContextCompat.getDrawable(requireContext(), R.drawable.ic_desplegar));
                    m_TextViewDescription.setVisibility(View.GONE);
                }
            });

            String description = "Publicado el " + video.getSnippet().getPublishedAt() + "\n\n" + video.getSnippet().getDescription();
            m_TextViewDescription.setText(description);

            //LIKE, DISLIKE AND SUBSCRIBE BUTTONS SETTING
            m_NumberOfLikes = v.findViewById(R.id.number_likes);
            m_NumberOfDislikes = v.findViewById(R.id.number_dislikes);

            m_NumberOfDislikes.setText(video.getStatistics().obtainDislikeCount());
            m_NumberOfLikes.setText(video.getStatistics().obtainLikeCount());

            m_LikedButton.setOnClickListener(x ->
            {
                if (main.isLoggedIn()) likeDislikeProcedure(main.loggedUser, video, true);
                else showLoginRequirementMessage();
            });

            m_DislikeButton.setOnClickListener(x ->
            {
                if (main.isLoggedIn()) likeDislikeProcedure(main.loggedUser, video, false);
                else showLoginRequirementMessage();
            });

            m_SubscribeText.setOnClickListener(x ->
            {
                if (main.isLoggedIn())
                {
                    if (main.loggedUser.isSubscribed(m_VideoChannel.getId()))
                    {
                        m_SubscribeText.setText(getResources().getString(R.string.suscribirme));
                        m_SubscribeText.setTextColor(getResources().getColor(R.color.color_red));

                        main.loggedUser.unsubscribe(m_VideoChannel.getId());
                        m_VideoChannel.getStatistics().unsubscribe();
                    }
                    else
                    {
                        m_SubscribeText.setText(getResources().getString(R.string.subscrito));
                        m_SubscribeText.setTextColor(getResources().getColor(R.color.color_grey_dark));

                        main.loggedUser.subscribe(m_VideoChannel.getId());
                        m_VideoChannel.getStatistics().subscribe();
                    }

                    String subsText = m_VideoChannel.getStatistics().getSubscriberCount() + " Suscriptores";
                    m_SubscribersCountText.setText(subsText);

                    updateData(m_VideoChannel, null);
                }
                else showLoginRequirementMessage();
            });

            v.findViewById(R.id.layout_download_vid).setOnClickListener(x ->
            {
                if (!main.isLoggedIn())
                {
                    showLoginRequirementMessage();
                    return;
                }

                new MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogTheme)
                        .setTitle("Bajarse el video")
                        .setMessage("Necesitas premium, campeon!")
                        .setPositiveButton(R.string.yes_download, null)
                        .setNegativeButton(R.string.no_download, null)
                        .setIcon(android.R.drawable.btn_star_big_on)
                        .show();
            });

            v.findViewById(R.id.layout_watchlater_vid).setOnClickListener(x ->
            {
                if (!main.isLoggedIn())
                {
                    showLoginRequirementMessage();
                    return;
                }

                if (main.loggedUser.inWatchlist(video.getId()))
                {
                    Utilities.ShowText(getContext(), "Ya tienes este video en tu lista de ver mas tarde!");
                }
                else
                {
                    new MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogTheme)
                            .setTitle("Guardar video para mas tarde")
                            .setMessage("Quieres guardar este video para tu lista de mas tarde?")
                            .setPositiveButton(R.string.yes, (dialog, which) ->
                            {
                                MainActivity.instance.loggedUser.addWatchLater(video.getId());
                                main.updateUserData();
                            })
                            .setNegativeButton(R.string.no, null)
                            .setIcon(android.R.drawable.btn_star_big_on)
                            .show();
                }
            });

            v.findViewById(R.id.imageview_watching_compartir).setOnClickListener(x ->
            {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hola, mira esto! " + Html.fromHtml("<font color='#FE2B3C'>" + "<br>www.youtube.com/watch?" + video.getEtag() + "</font>"));
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, "Comparte este maravilloso enlace!");
                startActivity(shareIntent);
            });

            //CHANNEL, VIEWS AND OTHER VIDEO INFORMATION

            String text = video.getStatistics().obtainViewCount() + " visualizaciones";
            ((TextView) v.findViewById(R.id.textview_watching_views)).setText(text);

            text = "hace " + video.getSnippet().obtainDaysBetween() + " dias";
            ((TextView) v.findViewById(R.id.textview_video_player_date)).setText(text);

            main.channelsDatabase.getData(video.getSnippet().getChannelId(), youtubeChannel ->
            {
                View.OnClickListener listener = x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Channel, youtubeChannel, "channel");
                String subsText = youtubeChannel.getStatistics().obtainSubscriberCount() + " Suscriptores";

                TextView channelName = v.findViewById(R.id.channel_name);
                channelName.setText(youtubeChannel.getSnippet().getTitle());
                channelName.setOnClickListener(listener);

                m_SubscribersCountText.setText(subsText);
                m_SubscribersCountText.setOnClickListener(listener);

                v.findViewById(R.id.watching_channel_image).setOnClickListener(listener);
                YoutubeManager.showUrlImage(youtubeChannel.obtainThumbnailUrl(YoutubeChannel.ThumbnailQuality.low), v.findViewById(R.id.watching_channel_image));

                if (main.isLoggedIn())
                {
                    boolean isSub = main.loggedUser.isSubscribed(youtubeChannel.getId());

                    m_SubscribeText.setText(isSub ? R.string.subscrito : R.string.suscribirme);
                    if (isSub) m_SubscribeText.setTextColor(getResources().getColor(R.color.color_grey_dark));
                }

                m_VideoChannel = youtubeChannel;
            });

            main.videosDatabase.updateDatabase(video);
        }

        //LOADING VIDEOS FROM DATABASE FOR RECOMMENDATIONS
        RecyclerView recyclerView = v.findViewById(R.id.recommended_videos_list);
        recyclerView.setLayoutManager(new

                LinearLayoutManager(getContext()));

        main.videosDatabase.getAllData(list ->
        {
            Collections.shuffle(list);
            recyclerView.setAdapter(new VideoRecyclerViewAdapter(list));
        });

        YouTubePlayerView youTubePlayerView = v.findViewById(R.id.youtube_player);

        youTubePlayerView.getYouTubePlayerWhenReady(youTubePlayer ->
        {
            youTubePlayer.cueVideo(m_VideoUrl, 0);
            youTubePlayer.play();
        });

        return v;
    }

    private void showLoginRequirementMessage()
    {
        Utilities.ShowText(getContext(), "Inicia sesión primero!");
    }

    private void updateData(YoutubeChannel channel, Video video)
    {
        MainActivity main = MainActivity.instance;

        if (video != null) main.videosDatabase.updateDatabase(video);
        if (channel != null) main.channelsDatabase.updateDatabase(channel);

        main.updateUserData();
    }

    private void likeDislikeProcedure(User user, Video video, boolean like)
    {
        String videoId = video.getId();

        setLikeVideo(user, video, like && !user.isLiked(videoId));

        setDislikeVideo(user, video, !like && !user.isDisliked(videoId));

        updateData(null, video);
    }

    private void setLikeVideo(User user, Video video, boolean like)
    {
        if (like)
        {
            user.like(video.getId());
            showLikeButton(true);
            video.getStatistics().incrementLikeCount();
        }
        else
        {
            user.unlike(video.getId());
            showLikeButton(false);
            video.getStatistics().decrementLikeCount();
        }

        m_NumberOfLikes.setText(video.getStatistics().obtainLikeCount());
    }

    private void setDislikeVideo(User user, Video video, boolean dislike)
    {
        if (dislike)
        {
            user.dislike(video.getId());
            showDislikeButton(true);
            video.getStatistics().incrementDislikeCount();
        }
        else
        {
            user.unalike(video.getId());
            showDislikeButton(false);
            video.getStatistics().decrementDislikeCount();
        }

        m_NumberOfDislikes.setText(video.getStatistics().obtainDislikeCount());
    }

    private void showLikeButton(boolean show) { m_LikedButton.setImageDrawable(ContextCompat.getDrawable(requireContext(), show ? R.drawable.ic_like_red : R.drawable.ic_like_outlined)); }

    private void showDislikeButton(boolean show) { m_DislikeButton.setImageDrawable(ContextCompat.getDrawable(requireContext(), show ? R.drawable.ic_dislike_red : R.drawable.ic_dislike_outlined)); }
}