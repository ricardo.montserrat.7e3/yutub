package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;

public class LibraryFragment extends Fragment
{
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_library, container, false);
        v.findViewById(R.id.history_button).setOnClickListener(x -> System.out.println("Go to the history fragment"));
        RecyclerView recyclerView = v.findViewById(R.id.recyclerview_library_history);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        VideoRecyclerViewAdapter videoListAdapter = new VideoRecyclerViewAdapter(new ArrayList<>(), true);

        recyclerView.setAdapter(videoListAdapter);

        v.findViewById(R.id.history_button).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.History));
        v.findViewById(R.id.my_videos_button).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.MyVideos));
        v.findViewById(R.id.watch_later_button).setOnClickListener(x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.WatchLater));

        List<String> history = MainActivity.instance.loggedUser.getHistory();
        if(history.size() > 0)
        {
            Database<Video> videoDatabase = MainActivity.instance.videosDatabase;
            history.forEach(videoId -> videoDatabase.getData(videoId, videoListAdapter::addVideo));
        }
        else
        {
            v.findViewById(R.id.textview_not_seen).setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        return v;
    }
}