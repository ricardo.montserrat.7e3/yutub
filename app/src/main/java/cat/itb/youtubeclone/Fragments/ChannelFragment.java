package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.YoutubeManager;

public class ChannelFragment extends Fragment
{
    public static final int[] channel_banners = {R.drawable.banner_clubpenguin, R.drawable.banner_penguindou, R.drawable.banner_xxpirakaxx, R.drawable.img_club_penguin};

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_channel, container, false);

        Bundle values = getArguments();

        MainActivity main = MainActivity.instance;

        //CREATE VIDEO LIST
        RecyclerView recyclerView = v.findViewById(R.id.channel_videos);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        VideoRecyclerViewAdapter videoListAdapter = new VideoRecyclerViewAdapter(new ArrayList<>());

        recyclerView.setAdapter(videoListAdapter);

        ((ImageView) v.findViewById(R.id.imageview_channel_banner)).setImageResource(channel_banners[(int) (Math.random() * channel_banners.length)]);

        if (values != null)
        {
            YoutubeChannel channel = (YoutubeChannel) values.getSerializable("channel");

            assert channel != null;
            ((TextView) v.findViewById(R.id.textview_channel_name)).setText(channel.getSnippet().getTitle());

            String text = channel.getStatistics().getSubscriberCount() + " suscriptores";
            ((TextView) v.findViewById(R.id.texview_channel_subscribers)).setText(text);

            YoutubeManager.showUrlImage(channel.obtainThumbnailUrl(YoutubeChannel.ThumbnailQuality.medium), v.findViewById(R.id.circleImageView_channel_profile_image));

            v.findViewById(R.id.layout_canal_editar).setVisibility(View.GONE);

            //ADD YOUTUBER'S VIDEOS
            main.videosDatabase.getDataWhere("/snippet/channelId", channel.getId(), list ->
            {
                list.forEach(videoListAdapter::addVideoNoUpdate);
                videoListAdapter.updateVideoList();
            }, true);
            YoutubeManager.showUrlImage(channel.obtainThumbnailUrl(YoutubeChannel.ThumbnailQuality.medium), v.findViewById(R.id.circleImageView_channel_profile_image));
        }
        else if (main.isLoggedIn())
        {
            //UPDATE CHANNEL INFORMATION ABOUT YOU
            ((TextView) v.findViewById(R.id.textview_channel_name)).setText(main.loggedUser.getUsername());
            ((TextView) v.findViewById(R.id.texview_channel_subscribers)).setText(main.loggedUser.getEmail());

            //ADD LIST OF LIKE VIDEOS
            Database<Video> database = main.videosDatabase;

            List<String> likedVideos = main.loggedUser.getLikedVideos();
            if (likedVideos.size() > 0) likedVideos.forEach(videoID -> database.getData(videoID, videoListAdapter::addVideo));
            else v.findViewById(R.id.textview_channel_not_liked).setVisibility(View.VISIBLE);

            //SHOW CHANNEL CONFIGURATION SINCE IS YOUR CHANNEL
            v.findViewById(R.id.layout_canal_editar).setVisibility(View.VISIBLE);
        }
        return v;
    }
}