package cat.itb.youtubeclone.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.User;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;

import static cat.itb.youtubeclone.Utilities.Utilities.isEmpty;
import static cat.itb.youtubeclone.Utilities.Utilities.isNotChecked;

public class RegisterFragment extends Fragment
{
    private TextInputEditText m_EmailUser, m_UsernameUser, m_UserPassword, m_UserPasswordRepeat;
    private MaterialCheckBox m_Terms;

    private final User m_NewUser = new User();

    private boolean m_UserExist;

    private MainActivity.FragmentTag m_LastFragment;

    public RegisterFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.layout_fragment_register, container, false);

        assert getArguments() != null;
        m_LastFragment = (MainActivity.FragmentTag) getArguments().getSerializable(LoginFragment.TAG_SERIALIZABLE);

        m_EmailUser = v.findViewById(R.id.inputedit_email);
        m_UsernameUser = v.findViewById(R.id.inputedit_username);

        m_UserPassword = v.findViewById(R.id.inputedit_password);
        m_UserPasswordRepeat = v.findViewById(R.id.inputedit_repeat_password);

        m_Terms = v.findViewById(R.id.checkbox_terms);

        v.findViewById(R.id.button_register).setOnClickListener(x -> register());

        View.OnClickListener goToLogin = x -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.Login, m_LastFragment, LoginFragment.TAG_SERIALIZABLE);

        v.findViewById(R.id.button_goto_login).setOnClickListener(goToLogin);
        v.findViewById(R.id.imagebutton_register_return).setOnClickListener(goToLogin);
        m_Terms.setOnClickListener(x ->
        {
            new AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme)
                    .setTitle("Aceptar terminos y condiciones")
                    .setMessage("Te robaremos todos los datos, seguro?")
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> m_Terms.setChecked(true))
                    .setNegativeButton(android.R.string.no, (dialog, which)  -> m_Terms.setChecked(false))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });
        return v;
    }

    private void register()
    {
        if (isEmpty(m_EmailUser, "¡Por favor, introduce un correo electrónico!")
                || isEmpty(m_UsernameUser, "¡Por favor, introduce un nombre de usuario!")
                || isEmpty(m_UserPassword, "¡Por favor, introduce una contraseña!")
                || isEmpty(m_UserPasswordRepeat, "¡Por favor, repite la contraseña!")
                || isNotChecked(m_Terms, "¡Por favor, lee y acepta los términos y condiciones!")) return;

        String userPassword = m_UserPassword.getText().toString();
        if (!userPassword.equals(m_UserPasswordRepeat.getText().toString())) return;

        String userName = m_UsernameUser.getText().toString(), userEmail = m_EmailUser.getText().toString();

        if (userEmail.contains("."))
        {
            if (userEmail.split("@").length != 2)
            {
                m_EmailUser.setError("¡Por favor, escribe tu correo electrónico correctamente!");
                return;
            }
        }
        else
        {
            m_EmailUser.setError("¡Por favor, escribe tu correo electrónico correctamente!");
            return;
        }

        MainActivity main = MainActivity.instance;

        main.usersDatabase.existPropertyWithValue("email", userEmail, exist -> { if (m_UserExist = exist) m_EmailUser.setError("Este correo electrónico ya está en uso"); });

        main.usersDatabase.existPropertyWithValue("username", userName, exist -> { if (m_UserExist = exist) m_UsernameUser.setError("Este nombre de usuario ya está en uso"); });

        if (m_UserExist) return;

        m_NewUser.setPassword(userPassword);
        m_NewUser.setUsername(userName);
        m_NewUser.setEmail(userEmail);

        main.usersDatabase.updateDatabase(m_NewUser);

        main.goToFragment(MainActivity.FragmentTag.Login);

        Utilities.ShowText(getContext(), userName + ", tu cuenta se ha creado exitosamente");
    }
}