package cat.itb.youtubeclone.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.R;

public class ConfigurationFragment extends Fragment
{
    public enum ConfigOption { ChangePassword, ChangeUsername }

    public static final String CONFIG_OPTION_TAG = "Coption";

    private MainActivity.FragmentTag m_LastFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_configuration, container, false);

        Bundle values = getArguments();

        MainActivity main = MainActivity.instance;

        if(values != null) m_LastFragment = (MainActivity.FragmentTag) getArguments().getSerializable(LoginFragment.TAG_SERIALIZABLE);

        v.findViewById(R.id.imagebutton_configuration_return).setOnClickListener(x ->
                main.goToFragment(m_LastFragment != null? m_LastFragment : MainActivity.FragmentTag.Videos));

        v.findViewById(R.id.textview_config_change_username).setOnClickListener(x ->
            main.goToFragment(MainActivity.FragmentTag.RetrievePassword, ConfigOption.ChangeUsername, CONFIG_OPTION_TAG));

        v.findViewById(R.id.textview_config_change_password).setOnClickListener(x ->
            main.goToFragment(MainActivity.FragmentTag.RetrievePassword, ConfigOption.ChangePassword, CONFIG_OPTION_TAG));

        return v;
    }
}