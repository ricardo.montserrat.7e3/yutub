package cat.itb.youtubeclone.Fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;

public class LikedVideosFragment extends Fragment
{
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_liked_videos, container, false);

        MainActivity main = MainActivity.instance;

        TextView notLikedVideosTextView = v.findViewById(R.id.textview_likedvideos_not_liked);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerview_liked_videos);

        if (main.isLoggedIn())
        {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            VideoRecyclerViewAdapter videoListAdapter = new VideoRecyclerViewAdapter(new ArrayList<>());

            recyclerView.setAdapter(videoListAdapter);

            v.findViewById(R.id.imageview_likedvideos_shuffle).setOnClickListener(x ->
            {
                int size = videoListAdapter.getItemCount();
                if(size > 0) videoListAdapter.clickOn((int) (Math.random() * size));
            });

            //ADD LIST OF LIKE VIDEOS
            Database<Video> database = main.videosDatabase;

            List<String> likedVideos = main.loggedUser.getLikedVideos();

            if(likedVideos.size() > 0) likedVideos.forEach(videoID -> database.getData(videoID, videoListAdapter::addVideo));
            else {
                notLikedVideosTextView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
        else
        {
            notLikedVideosTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            notLikedVideosTextView.setText(getText(R.string.necesitas_estar_logueado));
            v.findViewById(R.id.imageview_likedvideos_shuffle).setOnClickListener(x -> Utilities.ShowText(getContext(), "Inicia sesión primero!"));
        }

        return v;
    }
}