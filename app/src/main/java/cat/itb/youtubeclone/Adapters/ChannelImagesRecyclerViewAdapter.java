package cat.itb.youtubeclone.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.YoutubeManager;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChannelImagesRecyclerViewAdapter extends RecyclerView.Adapter<ChannelImagesRecyclerViewAdapter.ViewHolder> {

    private List<YoutubeChannel> m_SubscriptionsChannel;

    public ChannelImagesRecyclerViewAdapter(List<YoutubeChannel> m_SubscriptionsChannel) {
        this.m_SubscriptionsChannel = m_SubscriptionsChannel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChannelImagesRecyclerViewAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_channel_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        YoutubeChannel channel = m_SubscriptionsChannel.get(position);
        YoutubeManager.showUrlImage(channel.obtainThumbnailUrl(YoutubeChannel.ThumbnailQuality.medium), holder.m_ChannelImg);

        holder.itemView.setOnClickListener(image -> {
            MainActivity.instance.goToFragment(MainActivity.FragmentTag.Channel, channel, "channel");
        });
    }

    @Override
    public int getItemCount() {
        return m_SubscriptionsChannel.size();
    }

    public void addChannel(YoutubeChannel channel)
    {
        m_SubscriptionsChannel.add(channel);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final CircleImageView m_ChannelImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            m_ChannelImg = itemView.findViewById(R.id.circleImageView_subscriptions_channel_image);
        }
    }
}
