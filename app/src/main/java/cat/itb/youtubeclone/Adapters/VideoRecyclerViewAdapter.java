package cat.itb.youtubeclone.Adapters;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.R;
import cat.itb.youtubeclone.Utilities.Utilities;
import cat.itb.youtubeclone.Utilities.YoutubeManager;

public class VideoRecyclerViewAdapter extends RecyclerView.Adapter<VideoRecyclerViewAdapter.VideoViewHolder>
{
    private List<Video> m_Videos;
    private final ArrayList<Video> m_Searches = new ArrayList<>();
    private boolean m_IsHorizontal;

    public static Utilities.GetDataListener<Video> defaulVideoClickListener = video -> MainActivity.instance.goToFragment(MainActivity.FragmentTag.VideoPlayer, video, "video");

    public VideoRecyclerViewAdapter(List<Video> m_Videos)
    {
        this.m_Videos = m_Videos;
        m_Searches.addAll(m_Videos);
    }

    public VideoRecyclerViewAdapter(List<Video> m_Videos, boolean horizontal)
    {
        this.m_Videos = m_Videos;
        this.m_IsHorizontal = horizontal;
        m_Searches.addAll(m_Videos);
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) { return new VideoViewHolder(LayoutInflater.from(parent.getContext()).inflate(m_IsHorizontal ? R.layout.item_layout_history_videos : R.layout.item_layout_fragment_videos, parent, false)); }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, int position)
    {
        Video currentVideo = m_Searches.get(position);

        holder.m_TitleView.setText(currentVideo.getSnippet().getTitle());

        YoutubeManager.showUrlImage(currentVideo.getSnippet().getThumbnails().getHigh().getUrl(), holder.m_Thumbnail);

        if (!m_IsHorizontal)
        {
            String views = currentVideo.getStatistics().obtainViewCount() + " vis.";
            holder.m_ViewsView.setText(views);

            holder.m_LikesView.setText(String.valueOf(currentVideo.getStatistics().obtainLikeCount()));
            holder.m_DislikesView.setText(String.valueOf(currentVideo.getStatistics().obtainDislikeCount()));

            holder.m_ReleasedDateView.setText(currentVideo.getSnippet().getPublishedAt());
            MainActivity.instance.channelsDatabase.getData(currentVideo.getSnippet().getChannelId(), channel ->
                    YoutubeManager.showUrlImage(channel.obtainThumbnailUrl(YoutubeChannel.ThumbnailQuality.low), holder.m_ChannelImage));
        }

        holder.itemView.setOnClickListener(x -> defaulVideoClickListener.onSuccess(m_Searches.get(position)));

    }

    @Override
    public int getItemCount() { return m_Searches.size(); }

    public void addVideo(Video video)
    {
        addVideoNoUpdate(video);
        updateVideoList();
    }

    public void clickOn(int videoPosition)
    {
        defaulVideoClickListener.onSuccess(m_Searches.get(videoPosition));
    }

    public void addVideoNoUpdate(Video video) { m_Videos.add(video); }

    public void updateVideoList()
    {
        m_Searches.clear();
        m_Searches.addAll(m_Videos);
        notifyDataSetChanged();
    }

    public void setVideos(List<Video> m_Videos)
    {
        this.m_Videos = m_Videos;
        notifyDataSetChanged();
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView m_TitleView, m_ReleasedDateView;
        private final TextView m_ViewsView, m_LikesView, m_DislikesView;
        private final ImageView m_ChannelImage;
        private final YouTubeThumbnailView m_Thumbnail;

        public VideoViewHolder(View view)
        {
            super(view);
            m_TitleView = view.findViewById(R.id.textview_video_title);
            m_ReleasedDateView = view.findViewById(R.id.textview_video_date);

            m_ViewsView = view.findViewById(R.id.textview_video_views);

            m_LikesView = view.findViewById(R.id.textview_video_likes);
            m_DislikesView = view.findViewById(R.id.textview_video_dislikes);

            m_ChannelImage = view.findViewById(R.id.channel_image);
            m_Thumbnail = view.findViewById(R.id.thumbnail_view);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void searchForItem(String query)
    {
        final String lowerQuery = query.toLowerCase();
        m_Searches.clear();
        m_Videos.forEach(video ->
        {
            if (video.getSnippet().getTitle().toLowerCase().contains(lowerQuery)) m_Searches.add(video);
        });
        notifyDataSetChanged();
    }
}