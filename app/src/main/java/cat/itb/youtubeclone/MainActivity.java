package cat.itb.youtubeclone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;

import cat.itb.youtubeclone.Adapters.VideoRecyclerViewAdapter;
import cat.itb.youtubeclone.Database.Database;
import cat.itb.youtubeclone.Database.DatabaseManager;
import cat.itb.youtubeclone.Fragments.ChannelFragment;
import cat.itb.youtubeclone.Fragments.ConfigurationFragment;
import cat.itb.youtubeclone.Fragments.HistoryFragment;
import cat.itb.youtubeclone.Fragments.LibraryFragment;
import cat.itb.youtubeclone.Fragments.LikedVideosFragment;
import cat.itb.youtubeclone.Fragments.LoginFragment;
import cat.itb.youtubeclone.Fragments.MyVideosFragment;
import cat.itb.youtubeclone.Fragments.RecuperarPasswordFragment;
import cat.itb.youtubeclone.Fragments.RegisterFragment;
import cat.itb.youtubeclone.Fragments.SubscriptionFragment;
import cat.itb.youtubeclone.Fragments.VideosFragment;
import cat.itb.youtubeclone.Fragments.WatchLaterFragment;
import cat.itb.youtubeclone.Fragments.WatchingVideoFragment;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.User;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.Utilities.Utilities;

public class MainActivity extends AppCompatActivity
{
    public static MainActivity instance;

    public enum FragmentTag
    {
        Videos, Library, Subscriptions, Login, Register, VideoPlayer,
        Channel, RetrievePassword, MyVideos, LikedVideos, History, AccountConfig, WatchLater
    }

    public Database<User> usersDatabase;
    public Database<Video> videosDatabase;

    public Database<YoutubeChannel> channelsDatabase;

    public VideoRecyclerViewAdapter adapter;
    private DrawerLayout m_DrawerLayout;

    private MenuItem m_TvItem;
    private MaterialToolbar m_TopAppBar;

    private BottomNavigationView m_BottomNavigationBar;

    private FragmentTag m_CurrentFragment;

    public User loggedUser;

    private final String TAG_USER_SERIALIZABLE = "user", TAG_FRAGMENT_SERIALIZABLE = "fragment";
    public final String SHARED_TAG_WAS_LOGGED = "userLogged";

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // LAUNCHER SCREEN
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_main);

        instance = this;

        // DRAWER SETTING
        m_TopAppBar = findViewById(R.id.top_app_toolbar);
        m_DrawerLayout = findViewById(R.id.drawer_layout);
        m_BottomNavigationBar = findViewById(R.id.bottom_navigation_view);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, m_DrawerLayout, m_TopAppBar, R.string.openNavDrawer, R.string.closeNavDrawer);

        m_DrawerLayout.addDrawerListener(actionBarDrawerToggle);

        NavigationView drawer = findViewById(R.id.drawer_navigation_view);
        drawer.setNavigationItemSelectedListener(item ->
        {
            switch (item.getItemId())
            {
                case R.id.item_my_channel:
                    goToFragment(isLoggedIn() ? FragmentTag.Channel : FragmentTag.Login);
                    break;
                case R.id.item_logout:
                    loggedUser = null;
                    goToFragment(FragmentTag.Login);
                    updateDrawerHeader(drawer);
                    break;
                case R.id.item_configuration:
                    goToFragment(isLoggedIn()? FragmentTag.AccountConfig : FragmentTag.Login);
                    break;
                case R.id.item_historial:
                    goToFragment(FragmentTag.History);
                    break;
                case R.id.item_liked_videos:
                    goToFragment(FragmentTag.LikedVideos);
                    break;
                case R.id.item_my_videos:
                    goToFragment(FragmentTag.MyVideos);
                    break;
                case R.id.item_watch_later:
                    goToFragment(FragmentTag.WatchLater);
                    break;
                default: return false;
            }
            closeDrawer();
            return false;
        });

        actionBarDrawerToggle.syncState();

        // TOP APP BAR MENU SETTING
        Menu toolMenu = m_TopAppBar.getMenu();

        m_TvItem = toolMenu.findItem(R.id.menu_item_enviartv);

        MenuItem searcher = toolMenu.findItem(R.id.menu_item_buscar);

        searcher.setOnActionExpandListener(new MenuItem.OnActionExpandListener()
        {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) { m_TvItem.setIcon(R.drawable.ic_microfono); return true; }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) { m_TvItem.setIcon(R.drawable.ic_enviar_tv); return true; }
        });

        //SEARCHING CONFIG
        SearchView searchView = (SearchView) searcher.getActionView();

        searchView.setQueryHint("Buscar un vídeo...");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query) { return adapter.getItemCount() > 0; }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onQueryTextChange(String newText)
            {
                if (m_CurrentFragment != FragmentTag.Videos) goToFragment(FragmentTag.Videos);
                adapter.searchForItem(newText);
                return false;
            }
        });

        // BOTTOM BAR NAVIGATION
        m_BottomNavigationBar.setOnNavigationItemSelectedListener(item ->
        {
            switch (item.getItemId())
            {
                case R.id.menu_item_inicio:
                    goToFragment(FragmentTag.Videos);
                    break;

                case R.id.menu_item_suscripciones:
                    if (isLoggedIn()) goToFragment(FragmentTag.Subscriptions);
                    else goToFragment(FragmentTag.Login, FragmentTag.Subscriptions, LoginFragment.TAG_SERIALIZABLE);
                    break;

                case R.id.menu_item_biblioteca:
                    if (isLoggedIn()) goToFragment(FragmentTag.Library);
                    else goToFragment(FragmentTag.Login, FragmentTag.Library, LoginFragment.TAG_SERIALIZABLE);
                    break;
                    
                default:
                    return false;
            }
            return true;
        });

        //DATA INITIALIZATION
        videosDatabase = DatabaseManager.getDatabase(Video.class);
        usersDatabase = DatabaseManager.getDatabase(User.class);
        channelsDatabase = DatabaseManager.getDatabase(YoutubeChannel.class);

        videosDatabase.initialize();
        usersDatabase.initialize();
        channelsDatabase.initialize();

        //CHECK INSTANCE IN CASE OF MOBILE WAS TURNED HORIZONTAL
        if (savedInstanceState != null)
        {
            loggedUser = (User) savedInstanceState.getSerializable(TAG_USER_SERIALIZABLE);
            goToFragment(FragmentTag.valueOf(savedInstanceState.getString(TAG_FRAGMENT_SERIALIZABLE)));
            updateDrawerHeader(drawer);
        }
        else
        {
            //CHECK IF USER IS LOGGED ON THE APP SO IT STARTS LOGGED IN
            SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
            if (preferences.contains(SHARED_TAG_WAS_LOGGED) && preferences.getBoolean(SHARED_TAG_WAS_LOGGED, false))
            {
                usersDatabase.getDataWhere("username", preferences.getString(LoginFragment.SHARED_TAG_USER, ""), user ->
                {
                    if (user != null)
                    {
                        loggedUser = user;
                        updateDrawerHeader(drawer);
                    }
                });
            }
            goToFragment(FragmentTag.Videos);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_USER_SERIALIZABLE, loggedUser);
        outState.putString(TAG_FRAGMENT_SERIALIZABLE, m_CurrentFragment.name());
        //Saving last logged in
        getPreferences(Context.MODE_PRIVATE).edit().putBoolean(SHARED_TAG_WAS_LOGGED, loggedUser != null).apply();
    }

    @Override
    public void onBackPressed()
    {
        switch (m_CurrentFragment)
        {
            case Videos: if (!closeDrawer()) super.onBackPressed(); break;
            case Register: case RetrievePassword:
            goToFragment(FragmentTag.Login, null, LoginFragment.TAG_SERIALIZABLE); break;
            default:
                goToFragment(FragmentTag.Videos); break;
        }
    }

    private boolean closeDrawer()
    {
        boolean isOpened = m_DrawerLayout.isDrawerOpen(GravityCompat.START);
        if (isOpened) m_DrawerLayout.closeDrawer(GravityCompat.START);
        return isOpened;
    }

    public void hideBars()
    {
        m_TopAppBar.setVisibility(View.GONE);
        m_BottomNavigationBar.setVisibility(View.GONE);
    }

    public void showBars()
    {
        m_TopAppBar.setVisibility(View.VISIBLE);
        m_BottomNavigationBar.setVisibility(View.VISIBLE);
    }

    public void goToFragment(FragmentTag fragmentTag) { goToFragment(fragmentTag, null, null); }

    public void goToFragment(FragmentTag fragment, Serializable data, String dataTag)
    {
        if (fragment == m_CurrentFragment
                && m_CurrentFragment != FragmentTag.VideoPlayer
                && m_CurrentFragment != FragmentTag.Channel) return;

        Fragment target;
        switch (fragment)
        {
            case Login:
                hideBars();
                target = new LoginFragment();
                if (data == null)
                {
                    data = m_CurrentFragment;
                    dataTag = LoginFragment.TAG_SERIALIZABLE;
                }
                break;
            case Register:
                hideBars();
                target = new RegisterFragment(); break;
            case Videos:
                showBars();
                target = new VideosFragment(); break;
            case Library:
                showBars();
                target = new LibraryFragment(); break;
            case Subscriptions:
                showBars();
                target = new SubscriptionFragment(); break;
            case VideoPlayer:
                hideBars();
                target = new WatchingVideoFragment(); break;
            case Channel:
                showBars();
                target = new ChannelFragment(); break;
            case RetrievePassword:
                hideBars();
                target = new RecuperarPasswordFragment(); break;
            case History:
                showBars();
                target = new HistoryFragment(); break;
            case MyVideos:
                showBars();
                target = new MyVideosFragment(); break;
            case LikedVideos:
                showBars();
                target = new LikedVideosFragment(); break;
            case AccountConfig:
                hideBars();
                target = new ConfigurationFragment();
                if (data == null)
                {
                    data = m_CurrentFragment;
                    dataTag = LoginFragment.TAG_SERIALIZABLE;
                }
                break;
            case WatchLater:
                showBars();
                target = new WatchLaterFragment(); break;
            default:
                Utilities.ShowText(instance, "El tag del fragmento no existe: " + fragment); return;
        }

        if (data != null)
        {
            Bundle values = new Bundle();
            values.putSerializable(dataTag, data);
            target.setArguments(values);
        }

        goToFragment(target);
        m_CurrentFragment = fragment;
    }

    public void goToFragment(Fragment newFragment) { getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_view_tag, newFragment).commit(); }

    public boolean isLoggedIn() { return loggedUser != null; }

    public void updateUserData() { if (isLoggedIn()) usersDatabase.updateDatabase(loggedUser); }

    public void updateDrawerHeader(NavigationView drawer)
    {
        View headerLayout = drawer.getHeaderView(0);
        if (loggedUser == null)
        {
            ((TextView) headerLayout.findViewById(R.id.textview_header_name)).setText(getResources().getString(R.string.iniciar_sesion));
            ((TextView) headerLayout.findViewById(R.id.textview_header_email)).setText("");
        }
        else
        {
            ((TextView) headerLayout.findViewById(R.id.textview_header_name)).setText(loggedUser.getUsername());
            ((TextView) headerLayout.findViewById(R.id.textview_header_email)).setText(loggedUser.getEmail());
        }
        drawer.getMenu().findItem(R.id.item_logout).setVisible(loggedUser != null);
    }
}