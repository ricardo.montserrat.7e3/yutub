package cat.itb.youtubeclone.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.widget.Toast;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class Utilities
{
    public static final String GOOGLE_KEY = "AIzaSyBnojvvO8CXpPiJnvCc-fMhZ2AXzHW9TI8";

    public interface GetDataListener<T> { void onSuccess(T value); }

    public static void ShowText(Context context, String text) { Toast.makeText(context, text, Toast.LENGTH_SHORT).show(); }

    public static boolean isNotChecked(MaterialCheckBox m_terms, String errorMessage)
    {
        boolean isNotChecked = !m_terms.isChecked();
        if (isNotChecked) m_terms.setError(errorMessage);
        return isNotChecked;
    }

    public static boolean isEmpty(TextInputEditText inputEditText, String errorMessage)
    {
        boolean isEmpty = inputEditText.getText().toString().isEmpty();
        if (isEmpty) inputEditText.setError(errorMessage);
        return isEmpty;
    }

    public static String generateCode(int length)
    {
        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) code.append(random.nextInt(10));
        return code.toString();
    }

    public static String addToStringInteger(String value, int adder) { return String.valueOf(Integer.parseInt(value) + adder); }

    public static String formatNumbers(String num){
        if (num.length() > 6) return num.substring(0, num.length() - 6) + "M";
        else if (num.length() > 3) return num.substring(0, num.length() -3) + "m";
        return num;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
