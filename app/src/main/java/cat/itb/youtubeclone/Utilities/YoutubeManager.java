package cat.itb.youtubeclone.Utilities;

import android.os.Build;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;

import cat.itb.youtubeclone.MainActivity;
import cat.itb.youtubeclone.Modal.Channel.YoutubeChannel;
import cat.itb.youtubeclone.Modal.Video.Video;
import cat.itb.youtubeclone.Modal.YoutubeResponse.YoutubeListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class YoutubeManager
{
    public static final String YOUTUBE_API_BASE_URL = "https://youtube.googleapis.com/youtube/v3/";

    public static final String YOUTUBE_RESPONSE_VIDEO_PARAMETERS = "&part=snippet&part=statistics&part=contentDetails&part=status&chart=mostPopular";

    public static final String YOUTUBE_RESPONSE_CHANNEL_PARAMETERS = "&part=snippet&part=statistics";

    private final YoutubeAPI youtubeAPI;

    public YoutubeManager()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(YOUTUBE_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        youtubeAPI = retrofit.create(YoutubeAPI.class);
    }

    public void uploadMostPopularVideos(final int quantity)
    {
        youtubeAPI.getVideos(quantity).enqueue(new Callback<YoutubeListResponse<Video>>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<YoutubeListResponse<Video>> call, Response<YoutubeListResponse<Video>> response)
            {
                if (response.isSuccessful()) response.body().getItems().forEach(video -> MainActivity.instance.videosDatabase.updateDatabase(video));
                System.out.println(response.code());
            }

            @Override
            public void onFailure(Call<YoutubeListResponse<Video>> call, Throwable t) { System.out.println(call.request().toString()); }
        });
    }

    public void uploadChannelByID(final String channelID)
    {
        youtubeAPI.getChannelById(channelID).enqueue(new Callback<YoutubeListResponse<YoutubeChannel>>()
        {
            @Override
            public void onResponse(Call<YoutubeListResponse<YoutubeChannel>> call, Response<YoutubeListResponse<YoutubeChannel>> response)
            {
                if (response.isSuccessful()) MainActivity.instance.channelsDatabase.updateDatabase(response.body().getItems().get(0));
            }

            @Override
            public void onFailure(Call<YoutubeListResponse<YoutubeChannel>> call, Throwable t) { System.out.println(call.request().toString()); }
        });
    }

    public interface YoutubeAPI
    {
        @GET("videos?key=" + Utilities.GOOGLE_KEY + YOUTUBE_RESPONSE_VIDEO_PARAMETERS)
        Call<YoutubeListResponse<Video>> getVideos(@Query(value = "maxResults", encoded = true) int maxResults);

        @GET("channels?key=" + Utilities.GOOGLE_KEY + YOUTUBE_RESPONSE_CHANNEL_PARAMETERS)
        Call<YoutubeListResponse<YoutubeChannel>> getChannelById(@Query(value = "id", encoded = true) String channelID);
    }

    public static void showUrlImage(final String imageUrl, ImageView imageView)
    {
        imageView.setImageBitmap(Utilities.getBitmapFromURL(imageUrl));
    }
}
